
/**
 * @file
 * A JavaScript file for the theme.
 */


(function (Drupal, $) {

  Drupal.behaviors.devvlyAccordion = {
    attach: function (context, settings) {
      let accordionItems = $('.block-type-accordion .field--name-field-paragraphs > .field__item');
      if (accordionItems.length > 0) {
        accordionItems.find('.field--name-field-text').slideUp();

        accordionItems.find('.field--name-field-title').once().click(function () {
          let t = $(this);
          if (t.closest('.paragraph--type--accordion-item').hasClass('active')) {
            t.siblings('.field--name-field-text').slideUp().closest('.paragraph--type--accordion-item').removeClass('active');
          }
          else {
            accordionItems.find('.field--name-field-text').slideUp();
            accordionItems.find('.paragraph--type--accordion-item').removeClass('active');
            t.siblings('.field--name-field-text').slideDown().closest('.paragraph--type--accordion-item').addClass('active');
          }
        });
      }
    }
  };

  // Tabs
  Drupal.behaviors.devvlyTabs = {
    attach: function (context, settings) {
      var tab = $('.block-type-tabs .paragraph--type--tab-item');
      if (tab.length) {
        var tabs = "<div class='cmml-tabs'>";
        tab.each(function (e) {
          var t = $(this);
          var id = t.attr('id');
          var tab_label = t.find('.field--name-field-label').text();
          if (e === 0) {
            tabs += "<div class='cmml-tab active' data-id='" + id + "'>" + tab_label + "</div>";
            t.parent('.field__item').addClass('active');
          }
          else {
            tabs += "<div class='cmml-tab' data-id='" + id + "'>" + tab_label + "</div>";
          }
        });
        tabs += "</div>";
        $('.block-type-tabs').once().prepend(tabs);

        $('.cmml-tab').once().click(function () {
          var t = $(this);
          if (!t.hasClass('active')) {
            t.siblings('.cmml-tab').removeClass('active');
            $('.block-type-tabs .field--name-field-items > .field__item').removeClass('active');
            t.addClass('active');
            $('#' + t.attr('data-id')).parent('.field__item').addClass('active');
          }
        });
      }
    }
  };


})(Drupal, jQuery);
