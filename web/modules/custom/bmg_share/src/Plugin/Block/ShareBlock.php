<?php

namespace Drupal\bmg_share\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ShareBlock' block.
 *
 * @Block(
 *  id = "share_block",
 *  admin_label = @Translation("Share block"),
 * )
 */
class ShareBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $path = \Drupal::service('path.current')->getPath();
    $request = \Drupal::request();
    $base_url = $request->getHost();
    if ($route = $request->attributes->get(\Symfony\Cmf\Component\Routing\RouteObjectInterface::ROUTE_OBJECT)) {
      $title = \Drupal::service('title_resolver')->getTitle($request, $route);
    }

    $output = '<a target="_blank" href="http://www.facebook.com/sharer.php?u=http://' . $base_url . $path . '&t=' . $title .'" class="share-it" id="share-facebook" ><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x fa-inverse"></i></span></a>';
    $output .= '<a href="http://twitter.com/share?url=http://' . $base_url . $path  . '&text=' . $title  . '&via=greenswardllc" class="share-it" id="share-twitter" target="_blank"><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span></a>';
    $output .= '<a target="_blank" href="https://plus.google.com/share?url=http://' . $base_url . $path . '" class="share-it" id="share-google"><span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-google-plus fa-stack-1x fa-inverse"></i></span></a>';

    $build = [];
    $build['share_block'] = [
      '#markup' => $output,
      '#attached' => [
          'library' => [
            'bmg_share/social-share',
            'bmg_share/font-awesome',
          ],
        ],
      ];

    return $build;
  }

}
